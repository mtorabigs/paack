## Requirement
* `node` version 16+ and `npm` version 8+. you can install from [here](https://nodejs.org/en/download/).

We are using ReactJS v17+ so before adding any third-party library, please be sure it is compatible with this version of ReactJS.

## Installation
* use this command to clone our git repo: `git clone https://gitlab.com/mtorabigs/paack.git`
* go to the `paack` folder by this: `cd paack`
* run `npm install`
* Check the `Available Scripts` to see what you can do here.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Directory Structure
The top level directory structure will be as follows:

* `assets` - global static assets such as images, svgs, company logo, etc.
* `components` - global shared/reusable components, such as layout (wrappers, navigation), form components, buttons
* `services` - JavaScript modules
* `store` - Global Redux store
* `utils` - Utilities, helpers, constants, and the like
* `views` - Can also be called "pages", the majority of the app would be contained here