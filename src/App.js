import React from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import Layout from "./components/layout/Layout";
import {paackStore} from "./store/paack.store";

import Deliveries from "./views/Deliveries/deliveries";
import {Delivery, ROUTE_DELIVERY} from "./views/Deliveries/delivery/delivery";
import {Provider} from "react-redux";

const App = () => (
    <Provider store={paackStore}>
        <Router>
            <Routes>
                <Route path="/" element={<Layout/>}>
                    <Route index element={<Deliveries/>}/>
                    <Route exact path={ROUTE_DELIVERY} element={<Delivery/>}/>
                </Route>
            </Routes>
        </Router>
    </Provider>
);

export default App;
