import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const deliveries = screen.getByText(/Paack!/i);
  expect(deliveries).toBeInTheDocument();
});
