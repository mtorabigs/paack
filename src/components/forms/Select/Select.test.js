import {render, screen} from "@testing-library/react";
import Select from "./Select";
import React from "react";

test('test select', () => {
    render(<Select />);
    const {container} = render(<Select />);
    expect(container.firstChild.type).toEqual('select-one')
});