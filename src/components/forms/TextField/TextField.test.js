import {render, screen} from "@testing-library/react";
import TextField from "./TextField";
import React from "react";

test('test select', () => {
    render(<TextField />);
    const {container} = render(<TextField />);
    expect(container.firstChild.type).toEqual('text')
});