import { Outlet, Link } from "react-router-dom";

const Layout = () => {
    return (
        <div style={{padding:'10px'}}>
            <h1>Paack!</h1>
            <Outlet />
        </div>
    )
};

export default Layout;