import { api} from "../api.rtk";


// Define a service using a base URL and expected endpoints
export const deliveriesApi = api.injectEndpoints({
    endpoints: (builder) => ({
        getDeliveries: builder.query({
            query: () => `deliveries`,
        }),
        getDelivery: builder.query({
            query: (id) => `deliveries/${id}`,
        }),
        finishDelivery: builder.mutation({
            query(body) {
                return {
                    url: `finishDelivery`,
                    method: 'POST',
                    body,
                }
            },
        }),
    }),
});

export const {useGetDeliveriesQuery,useGetDeliveryQuery,useFinishDeliveryMutation} = deliveriesApi;