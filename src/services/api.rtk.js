import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import {SERVER_BASE_URL} from "../utils/constants/server.const";

export const api = createApi({
    baseQuery: fetchBaseQuery({
        baseUrl: SERVER_BASE_URL,
        prepareHeaders:  (headers) => {
            return headers
        },
    }),
    endpoints: () => ({}),
    reducerPath: 'api',
});