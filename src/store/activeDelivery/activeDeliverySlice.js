import { createSlice } from '@reduxjs/toolkit'

export const activeDeliverySlice = createSlice({
    name: 'activeDelivery',
    initialState: {
        activeId: -1,
    },
    reducers: {
        activeDelivery: (state,action) => {
            state.activeId = action.payload;
        },
        deActiveDelivery: (state) => {
            state.activeId = -1;
        }
    },
});

export const { activeDelivery, deActiveDelivery } = activeDeliverySlice.actions;

export default activeDeliverySlice.reducer;