import { configureStore } from '@reduxjs/toolkit'
import { setupListeners } from '@reduxjs/toolkit/query'
import { deliveriesApi} from "../services/Deliveries/Deliveries.api";
import activeDeliveryReducer from "./activeDelivery/activeDeliverySlice"
export const paackStore = configureStore({
    reducer: {
        [deliveriesApi.reducerPath]: deliveriesApi.reducer,
        activeDelivery: activeDeliveryReducer
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(deliveriesApi.middleware),
});

setupListeners(paackStore.dispatch);