import * as React from "react";
import {Link} from "react-router-dom";

function Delivery(props) {
    return <>
        <Link to={'/delivery/' + props.data.id}>
            {props.data.id}
        </Link><br/>
        <b>City</b>: {props.data.city}<br/>
        <b>Address</b>: {props.data.address}<br/>
        <b>ZipCode</b>: {props.data.zipCode}<br/>
        <b>Latitude</b>: {props.data.latitude}<br/>
        <b>Longitude</b>: {props.data.longitude}<br/>
        <b>Customer</b>: {props.data.customer}<br/>
        <hr/>

    </>
}

export default Delivery;