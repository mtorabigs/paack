import * as React from 'react'
import {useGetDeliveriesQuery} from "../../services/Deliveries/Deliveries.api";
import Delivery from "./components/delivery.component";

export const ROUTE_DELIVERIES = '/';

export default function Deliveries() {
    const {data, error} = useGetDeliveriesQuery();

    return (
        <div>
            {error ? (
                <>Oh no, there was an error</>
            ) : data ? (
                <div>
                    <h2>Deliveries:</h2>
                    {
                        data.map(function (delivery) {
                            return <Delivery key={delivery.id} data={delivery}/>;
                        })
                    }
                </div>
            ) : null}
        </div>
    )
}