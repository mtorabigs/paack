import {render, screen, waitFor} from '@testing-library/react';
import {renderHook} from "@testing-library/react-hooks";
import {Provider} from "react-redux";
import React from "react";
import {deliveriesApi, useGetDeliveriesQuery} from "../../services/Deliveries/Deliveries.api";
import {paackStore} from "../../store/paack.store";
import {setupServer} from "msw/node";
import {rest} from "msw";
import {SERVER_BASE_URL} from "../../utils/constants/server.const";
import {BrowserRouter as Router} from "react-router-dom";
import Deliveries from "./deliveries";

describe('Deliveries', () => {
    it('provider must work', async () => {
        const server = setupServer(
            rest.get(SERVER_BASE_URL + '/deliveries', (req, res, ctx) => {
                return res(ctx.json([{id: 12}]))
            }),
        );
        server.listen();
        const wrapper = ({children}) => {
            return <Provider store={paackStore}>{children}</Provider>;
        };

        const {result, waitForNextUpdate} = renderHook(
            () => useGetDeliveriesQuery(),
            {wrapper}
        );

        const initialResponse = result.current;
        expect(initialResponse.data).toBeUndefined();

        expect(initialResponse.isLoading).toBe(true);

        await waitForNextUpdate({timeout: 5000});

        const nextResponse = result.current;
        expect(nextResponse.data).not.toBeUndefined();
        expect(nextResponse.isLoading).toBe(false);
        expect(nextResponse.isSuccess).toBe(true);
        server.close()
    });
    it('loads and displays delivery', async () => {
        const server = setupServer(
            rest.get(SERVER_BASE_URL + '/deliveries', (req, res, ctx) => {
                return res(ctx.json([{id: 12}]))
            }),
        );
        server.listen();
        render(<Provider store={paackStore}>
            <Router>
                <Deliveries/>
            </Router>

        </Provider>);

        await waitFor(() => screen.getByText(/Deliveries/i));
        const deliveries = screen.getByText(/Deliveries/i);
        expect(deliveries).toBeInTheDocument();
        server.close()
    });
    it('show error', async () => {
        const server = setupServer(
            rest.get(SERVER_BASE_URL + '/deliveries', (req, res, ctx) => {
                return res(ctx.status(404))
            }),
        );
        server.listen();

        paackStore.dispatch(deliveriesApi.util.resetApiState());

        render(<Provider store={paackStore}>
            <Router>
                <Deliveries/>
            </Router>

        </Provider>);

        await waitFor(() => screen.getByText(/Oh no, there was an error/i));
        const error = screen.getByText(/Oh no, there was an error/i);
        expect(error).toBeInTheDocument();
        server.close()
    });
});


