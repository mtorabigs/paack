import * as React from 'react'
import {useFinishDeliveryMutation, useGetDeliveryQuery} from "../../../services/Deliveries/Deliveries.api";
import {useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import locationHelper from "../../../utils/helpers/Location.helper"
import {activeDelivery, deActiveDelivery} from "../../../store/activeDelivery/activeDeliverySlice";

export const ROUTE_DELIVERY = '/delivery/:deliveryId';

export function Delivery() {
    const {deliveryId} = useParams();
    const {data, error} = useGetDeliveryQuery(deliveryId);
    const activeDeliveryId = useSelector((state) => state.activeDelivery.activeId);
    const dispatch = useDispatch();
    const [finishDelivery] = useFinishDeliveryMutation();

    function makeActive() {
        if (activeDeliveryId === -1)
            dispatch(activeDelivery(deliveryId));
    }

    function markAsDelivered() {
        dispatch(deActiveDelivery());
        locationHelper((location)=>{
            finishDelivery({
                deliveryId: deliveryId,
                status: 'delivered',
                latitude: location.coords.latitude,
                longitude: location.coords.longitude,
            });
        });
    }

    function markAsUndelivered() {
        dispatch(deActiveDelivery());
        locationHelper((location)=>{
            finishDelivery({
                deliveryId: deliveryId,
                status: 'undelivered',
                latitude: location.coords.latitude,
                longitude: location.coords.longitude,
            });
        });
    }

    return (
        <div>
            {error ? (<>Oh no, there was an error</>)
                : data ? (
                    <>
                        {(activeDeliveryId === data.id) ? (
                            <>
                                <button onClick={markAsDelivered}>Delivered</button>
                                <button style={{marginLeft: '10px'}} onClick={markAsUndelivered}>Undelivered</button>
                            </>
                        ) : ''}<br/>
                        {(activeDeliveryId === -1) ? <button onClick={makeActive}>Make active</button> : ''}<br/>
                        <p>
                            <b>City</b>: <span>{data.city}</span><br/>
                            <b>Address</b>: <span>{data.address}</span><br/>
                            <b>ZipCode</b>: <span>{data.zipCode}</span><br/>
                            <b>Latitude</b>: <span>{data.latitude}</span><br/>
                            <b>Longitude</b>: <span>{data.longitude}</span><br/>
                            <b>Customer</b>: <span>{data.customer}</span><br/>
                        </p>
                    </>
                ) : null}
        </div>
    )
}