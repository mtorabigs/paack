import {renderHook} from "@testing-library/react-hooks";
import {Provider} from "react-redux";
import React from "react";
import {render, screen, waitFor,fireEvent} from '@testing-library/react'
import {rest} from 'msw'
import {setupServer} from 'msw/node'
import fetchMock from "jest-fetch-mock";
import {deliveriesApi, useGetDeliveryQuery,useFinishDeliveryMutation} from "../../../services/Deliveries/Deliveries.api";
import {paackStore} from "../../../store/paack.store";
import {Delivery, ROUTE_DELIVERY} from "./delivery";
import {SERVER_BASE_URL} from "../../../utils/constants/server.const";
import {BrowserRouter as Router} from "react-router-dom";
import {activeDelivery, deActiveDelivery} from "../../../store/activeDelivery/activeDeliverySlice";

const address = 'some address';
const successGetServer = setupServer(
    rest.get(SERVER_BASE_URL + '/deliveries/undefined', (req, res, ctx) => {
        return res(ctx.json({address: address}))
    }),
);
const successPostServer = setupServer(
    rest.post(SERVER_BASE_URL + '/finishDelivery', (req, res, ctx) => {
        return res(ctx.status(200))
    }),
);
const failureServer = setupServer(
    rest.get(SERVER_BASE_URL + '/deliveries/undefined', (req, res, ctx) => {
        return res(ctx.status(404))
    }),
);
const componentWrapper  = (<Provider store={paackStore}>
    <Router>
        <Delivery path={ROUTE_DELIVERY}/>
    </Router>

</Provider>);
const mockGeolocation = {
    getCurrentPosition: jest.fn()
        .mockImplementationOnce((success) => Promise.resolve(success({
            coords: {
                latitude: 6,
                longitude: 45.3
            }
        })))
};
const mockPermission = {
    query: (result) => Promise.resolve({
        state:'granted'
    })
};
global.navigator.geolocation = mockGeolocation;
global.navigator.permissions = mockPermission;

describe('Delivery', () => {

    it('get delivery hook must work ', () => {
        successGetServer.listen();
        const wrapper = ({children}) => {
            return <Provider store={paackStore}>{children}</Provider>;
        };

        const {result, waitForNextUpdate} = renderHook(
            () => useGetDeliveryQuery(undefined),
            {wrapper}
        );

        const initialResponse = result.current;
        expect(initialResponse.data).toBeUndefined();

        expect(initialResponse.isLoading).toBe(true);
        successGetServer.close()
    });

    it('loads and displays delivery', async () => {
        successGetServer.listen();
        paackStore.dispatch(deliveriesApi.util.resetApiState());
        render(componentWrapper);

        await waitFor(() => screen.getByText(address));

        expect(screen.getByText(address)).toBeInTheDocument();
        successGetServer.close()
    });
    it('show error', async () => {
        failureServer.listen();

        paackStore.dispatch(deliveriesApi.util.resetApiState());

        render(componentWrapper);

        await waitFor(() => screen.getByText(/Oh no, there was an error/i));
        const error = screen.getByText(/Oh no, there was an error/i);
        expect(error).toBeInTheDocument();
        failureServer.close()
    });
    it('mark as active', async () => {
        successGetServer.listen();

        paackStore.dispatch(deActiveDelivery());

        render(componentWrapper);

        await waitFor(() => screen.getByText(/Make active/i));
        const button = screen.getByText(/Make active/i);
        fireEvent.click(button);
        expect(screen.getByText(/Undelivered/i)).toBeInTheDocument();
        successGetServer.close()
    });

    it('mark as delivered', async () => {
        successPostServer.listen();

        paackStore.dispatch(deActiveDelivery());
        paackStore.dispatch(activeDelivery(undefined));
        render(componentWrapper);

        await waitFor(() => screen.getByText('Delivered'));
        const button = screen.getByText('Delivered');
        fireEvent.click(button);
        expect(screen.getByText(/Make active/i)).toBeInTheDocument();
        successPostServer.close()
    });

    it('mark as undelivered', async () => {
        successPostServer.listen();

        paackStore.dispatch(deActiveDelivery());
        paackStore.dispatch(activeDelivery(undefined));
        render(componentWrapper);

        await waitFor(() => screen.getByText('Undelivered'));
        const button = screen.getByText('Undelivered');
        fireEvent.click(button);
        expect(screen.getByText(/Make active/i)).toBeInTheDocument();
        successPostServer.close()
    });

});




